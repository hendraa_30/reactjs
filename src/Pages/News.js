import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import { IconButton, Typography, Button} from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import AddIcon from '@material-ui/icons/Add';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  icon:{
      fontSize:24,
  },
  header:{
      fontWeight:'bold'
  },
  posisi:{
      textAlign:'right'
  },
  button:{
      backgroundColor:'purple'
  }
});





export default function News() {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);
  const [dialogDelete, setDialogDelete] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };
  const handleDialogDelete = () => {
    setDialogDelete(true);
};
const handleCloseDialogDelete = () => {
    setDialogDelete(false);
};

  return (
    <div>
      <TableContainer component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
            <TableRow>
              <TableCell colSpan={6}>
              <Grid container>
                <Grid item md={9}>
                   <Typography>Daftar Buku</Typography>
               </Grid>
               <Grid item md={3} className={classes.posisi}>
                   <Button onClick={handleClickOpen}><AddIcon/>Tambah</Button>
               </Grid>
                </Grid>

              </TableCell>
                
               
            </TableRow>
          <TableRow>
            <TableCell>No</TableCell>
            <TableCell align="right">Penulis</TableCell>
            <TableCell align="right">Judul</TableCell>
            <TableCell align="right">Konten</TableCell>
            <TableCell align="right">Date</TableCell>
            <TableCell align="right">Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
         <TableRow>
             <TableCell>1</TableCell>
             <TableCell align="right">Oda</TableCell>
             <TableCell align="right">One piece</TableCell>
             <TableCell align="right">Anime</TableCell>
             <TableCell align="right">2 januari 2010</TableCell>
             <TableCell align="right">
                 <IconButton>
                     <CreateIcon className={classes.icon} onClick={handleClickOpen}/>
                    
                 </IconButton>
                 <IconButton>
                     <DeleteIcon className={classes.icon} onClick={handleDialogDelete}/>
                 </IconButton>
             </TableCell>
         </TableRow>
        </TableBody>
      </Table>
   
    </TableContainer>

    <Dialog 
      open={open} 
      onClose={handleClose} 
      aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Subscribe</DialogTitle>
      <DialogContent>
        <Grid container>
          <Grid md={12} className={classes.dialogStyle}>
              <TextField
                autoFocus
                margin="dense"
                id="outlined-basic"
                label="Penulis"
                variant="outlined"
                fullWidth
              />
          </Grid>
          <Grid md={12} className={classes.dialogStyle}>
              <TextField
                autoFocus
                margin="dense"
                id="outlined-basic"
                label="Judul Buku"
                variant="outlined"
                fullWidth
              />
          </Grid>
          <Grid md={12} className={classes.dialogStyle}>
              <TextField
                autoFocus
                margin="dense"
                id="outlined-basic"
                label="Konten"
                variant="outlined"
                fullWidth
              />
          </Grid>
        </Grid>

      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose} color="primary">
          Cancel
        </Button>
        <Button onClick={handleClose} color="primary">
          Subscribe
        </Button>
      </DialogActions>
    </Dialog>

    <Dialog
                open={dialogDelete}
                onClose={handleCloseDialogDelete}
                aria-labelledby='form-dialog-title'
            >
                <DialogTitle id='form-dialog-title'>Hapus Buku</DialogTitle>
                <DialogContent>
                    <Typography>
                        Apakah Anda Yakin Ingin Menghapus Data Ini?
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseDialogDelete} color='primary'>
                        Cancel
                    </Button>
                    <Button color='primary'>Submit</Button>
                </DialogActions>
            </Dialog>

    </div>


    

    
 
    
  );
}