import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
// component
import Header from '../Components/Header';
// main Pages
import Home from '../Pages/Home';
import News from '../Pages/News';
import Profile from '../Pages/Profile'

import { Box } from '@material-ui/core';

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    },
}));

function Routers(props) {
    const classes = useStyles();
    return (
        // <div className={classes.root}>
        <Router>
          
            <Header />
            <main className={classes.content}>
                <Route exact path='/' component={Home} />
                <Route exact path='/News' component={News} />
                <Route exact path ="/Profil" component={Profile}/>
              
            </main>
            {/* <Footer /> */}
        </Router>
        // </div>
    );
}

export default Routers;
